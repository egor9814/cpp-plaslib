//
// Created by egor9814 on 06 Aug 2021.
//

#include "reader.hpp"
#include <iostream>
#include <sstream>

namespace pla {

	FileReader::FileReader(File &out) : out(&out) {}

	void FileReader::read(std::istream &in) {
		std::string line;
		size_t products{0};
		do {
			std::getline(in, line);

			if (line.starts_with(".i ")) {
				char *endptr{nullptr};
				auto str = line.data() + 3;
				inputs = strtol(str, &endptr, 10);
				if (endptr == str) {
					throw std::runtime_error("invalid inputs count format: " + line);
				}
				out->inputLabels.resize(inputs);
			} else if (line.starts_with(".o ")) {
				char *endptr{nullptr};
				auto str = line.data() + 3;
				outputs = strtol(str, &endptr, 10);
				if (endptr == str) {
					throw std::runtime_error("invalid outputs count format: " + line);
				}
				out->outputLabels.resize(outputs);
			} else if (line.starts_with(".ilb ")) {
				std::stringstream ss{line.data() + 5};
				for (auto &it : out->inputLabels) {
					ss >> it;
				}
			} else if (line.starts_with(".ob ")) {
				std::stringstream ss{line.data() + 4};
				for (auto &it : out->outputLabels) {
					ss >> it;
				}
			} else if (line.starts_with(".p ")) {
				char *endptr{nullptr};
				auto str = line.data() + 3;
				productsCount = strtol(str, &endptr, 10);
				if (endptr == str) {
					throw std::runtime_error("invalid products count format: " + line);
				}
			} else if (line.starts_with(".e")) {
				if (products < productsCount) {
					throw std::runtime_error("not enough products");
				}
				break;
			} else {
				std::stringstream ss{line};
				std::string s1, s2;
				ss >> s1 >> s2;
				if (s2.empty()) {
					s2 = "1";
				}
				for (auto &it : s1) {
					if (it == '~') {
						it = '0';
					}
				}
				for (auto &it : s2) {
					if (it == '~') {
						it = '0';
					}
				}
				BitInterval bi1{s1.data()};
				if (bi1.getLength() != s1.size()) {
					throw std::runtime_error("cannot parse bit interval '" + s1 + "'");
				}
				BitInterval bi2{s2.data()};
				if (bi2.getLength() != s2.size()) {
					throw std::runtime_error("cannot parse bit interval '" + s2 + "'");
				}

				auto outsCount = (~bi2.getDnc() & bi2.getVector()).getWeight();
				std::vector<BitInterval> outs;
				outs.reserve(outsCount);
				if (outsCount > 1) {
					throw std::runtime_error("multiple output not supported yet");
				}
				outs.push_back(bi2);
				products++;
				for (const auto &output : outs) {
					out->products->append(bi1, output);
				}
			}

			if (line.empty()) continue;
		} while (!in.eof());
	}

}
