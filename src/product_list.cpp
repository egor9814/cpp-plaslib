//
// Created by egor9814 on 05 Aug 2021.
//

#include <pla.hpp>

namespace pla {

	/*bool ProductList::contains(const ProductPtr &p) const {
		return data.contains(p);
	}*/

	void ProductList::append(const ProductPtr &p) {
//		if (!data.contains(p))
//			data.insert(p);
		if (auto it = std::find_if(data.begin(), data.end(), [&p](const ProductPtr &it) -> bool {
			return p->input == it->input && p->output == it->output;
		}); it == data.end()) {
			data.push_back(p);
		}
	}

	ProductPtr ProductList::append(const BitInterval &input, const BitInterval &output) {
		auto p = Product::create(input, output);
		append(p);
		return p;
	}

	size_t ProductList::size() const {
		return data.size();
	}

	BitIntervalMap<std::vector<BitInterval>> ProductList::group() const {
		BitIntervalMap<std::vector<BitInterval>> result;
		BitIntervalMap<BitIntervalMap<int>> r;
		for (const auto &it : data) {
			auto &bis = r[it->output];
			bis[it->input]++;
		}
		for (const auto &[k, v] : r) {
			std::vector<BitInterval> intervals;
			intervals.reserve(v.size());
			for (const auto &it : v) {
				intervals.push_back(it.first);
			}
			result[k] = std::move(intervals);
		}
		return result;
	}

	ProductList::ProductList(size_t capacity) {
		data.reserve(capacity);
	}

	ProductListPtr ProductList::create(size_t capacity) {
		return std::make_shared<ProductList>(capacity);
	}

}
