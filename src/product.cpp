//
// Created by egor9814 on 05 Aug 2021.
//

#include <pla.hpp>

namespace pla {

	Product::Product(BitInterval input, BitInterval output) noexcept
			: input(std::move(input)), output(std::move(output)) {}

	ProductPtr Product::create(const BitInterval &input, const BitInterval &output) {
		return std::make_shared<Product>(input, output);
	}

}
