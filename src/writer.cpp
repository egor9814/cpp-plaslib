//
// Created by egor9814 on 06 Aug 2021.
//

#include "writer.hpp"
#include <iostream>

namespace pla {

	FileWriter::FileWriter(const File &in) : in(&in) {}

	void FileWriter::write(std::ostream &out) const {
		out << ".i " << in->inputLabels.size() << std::endl;
		out << ".o " << in->outputLabels.size() << std::endl;

		out << ".ilb";
		for (const auto &it : in->inputLabels) {
			out << ' ' << it;
		}
		out << std::endl;

		out << ".ob";
		for (const auto &it : in->outputLabels) {
			out << ' ' << it;
		}
		out << std::endl;

		out << ".p " << in->products->size() << std::endl;
		for (const auto &it : in->products->data) {
			out << it->input.toString() << ' ' << it->output.toString() << std::endl;
		}
		out << ".e" << std::endl;
	}

}
