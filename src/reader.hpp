//
// Created by egor9814 on 06 Aug 2021.
//

#ifndef PLASLIB_READER_HPP
#define PLASLIB_READER_HPP

#include <pla.hpp>

namespace pla {

	struct FileReader {
		File *out;
		size_t inputs{0}, outputs{0}, productsCount{0};

		explicit FileReader(File &out);

		void read(std::istream &in);
	};

}

#endif //PLASLIB_READER_HPP
