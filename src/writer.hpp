//
// Created by egor9814 on 06 Aug 2021.
//

#ifndef PLASLIB_WRITER_HPP
#define PLASLIB_WRITER_HPP

#include <pla.hpp>

namespace pla {

	struct FileWriter {
		File const *in;

		explicit FileWriter(const File &in);

		void write(std::ostream &out) const;
	};

}

#endif //PLASLIB_WRITER_HPP
