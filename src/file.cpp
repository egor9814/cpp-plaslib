//
// Created by egor9814 on 06 Aug 2021.
//

#include <pla.hpp>
#include "reader.hpp"
#include "writer.hpp"

namespace pla {

	File::File(size_t capacity) : products(ProductList::create(capacity)) {}

	BitIntervalMap<DNF> File::toDNFs() const {
		BitIntervalMap<DNF> result;
		for (const auto &[output, product] : products->group()) {
			result[output] = product;
		}
		return result;
	}

	std::istream &operator>>(std::istream &in, File &f) {
		FileReader r{f};
		r.read(in);
		return in;
	}

	std::ostream &operator<<(std::ostream &out, const File &f) {
		FileWriter w{f};
		w.write(out);
		return out;
	}

}
