//
// Created by egor9814 on 04 Aug 2021.
//

#include "common.hpp"

PLATestUnit(Xor5)

	AutoTestCase() {
		using namespace bitops::literals;
		if (auto r = openPLA("xor5.pla"); r.isError()) {
			static constexpr bool notError = false;
			TestCheckMessage(notError, r.getError());
		} else {
			const auto &pla = r.getFile();
			TestCheck(pla.inputLabels.size() == 5);
			TestCheck(pla.inputLabels[0] == "d");
			TestCheck(pla.inputLabels[1] == "c");
			TestCheck(pla.inputLabels[2] == "b");
			TestCheck(pla.inputLabels[3] == "a");
			TestCheck(pla.inputLabels[4] == "e");
			TestCheck(pla.outputLabels.size() == 1);
			TestCheck(pla.outputLabels[0] == "xor5");
			TestCheck(pla.products->size() == 16);
			TestCheck(pla.products->data[0]->input == "11111"_bi);
			TestCheck(pla.products->data[0]->output == "1"_bi);
			TestCheck(pla.products->data[1]->input == "01110"_bi);
			TestCheck(pla.products->data[1]->output == "1"_bi);
			TestCheck(pla.products->data[2]->input == "10110"_bi);
			TestCheck(pla.products->data[2]->output == "1"_bi);
			TestCheck(pla.products->data[3]->input == "00111"_bi);
			TestCheck(pla.products->data[3]->output == "1"_bi);
			TestCheck(pla.products->data[4]->input == "11010"_bi);
			TestCheck(pla.products->data[4]->output == "1"_bi);
			TestCheck(pla.products->data[5]->input == "01011"_bi);
			TestCheck(pla.products->data[5]->output == "1"_bi);
			TestCheck(pla.products->data[6]->input == "10011"_bi);
			TestCheck(pla.products->data[6]->output == "1"_bi);
			TestCheck(pla.products->data[7]->input == "00010"_bi);
			TestCheck(pla.products->data[7]->output == "1"_bi);
			TestCheck(pla.products->data[8]->input == "11100"_bi);
			TestCheck(pla.products->data[8]->output == "1"_bi);
			TestCheck(pla.products->data[9]->input == "01101"_bi);
			TestCheck(pla.products->data[9]->output == "1"_bi);
			TestCheck(pla.products->data[10]->input == "10101"_bi);
			TestCheck(pla.products->data[10]->output == "1"_bi);
			TestCheck(pla.products->data[11]->input == "00100"_bi);
			TestCheck(pla.products->data[11]->output == "1"_bi);
			TestCheck(pla.products->data[12]->input == "11001"_bi);
			TestCheck(pla.products->data[12]->output == "1"_bi);
			TestCheck(pla.products->data[13]->input == "01000"_bi);
			TestCheck(pla.products->data[13]->output == "1"_bi);
			TestCheck(pla.products->data[14]->input == "10000"_bi);
			TestCheck(pla.products->data[14]->output == "1"_bi);
			TestCheck(pla.products->data[15]->input == "00001"_bi);
			TestCheck(pla.products->data[15]->output == "1"_bi);
		}
	}

EndTestUnit(DNF0)
