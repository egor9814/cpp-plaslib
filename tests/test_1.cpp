//
// Created by egor9814 on 04 Aug 2021.
//

#include "common.hpp"

PLATestUnit(DNF1)

	AutoTestCase() {
		using namespace bitops::literals;
		if (auto r = openPLA("dnf_1.pla"); r.isError()) {
			static constexpr bool notError = false;
			TestCheckMessage(notError, r.getError());
		} else {
			const auto &pla = r.getFile();
			TestCheck(pla.inputLabels.size() == 6);
			TestCheck(pla.outputLabels.empty());
			TestCheck(pla.products->size() == 13);
			TestCheck(pla.products->data[0]->input == "1----0"_bi);
			TestCheck(pla.products->data[0]->output == "1"_bi);
			TestCheck(pla.products->data[1]->input == "1-1-1-"_bi);
			TestCheck(pla.products->data[1]->output == "1"_bi);
			TestCheck(pla.products->data[2]->input == "10--11"_bi);
			TestCheck(pla.products->data[2]->output == "1"_bi);
			TestCheck(pla.products->data[3]->input == "1---01"_bi);
			TestCheck(pla.products->data[3]->output == "1"_bi);
			TestCheck(pla.products->data[4]->input == "110--1"_bi);
			TestCheck(pla.products->data[4]->output == "1"_bi);
			TestCheck(pla.products->data[5]->input == "01---1"_bi);
			TestCheck(pla.products->data[5]->output == "1"_bi);
			TestCheck(pla.products->data[6]->input == "0---01"_bi);
			TestCheck(pla.products->data[6]->output == "1"_bi);
			TestCheck(pla.products->data[7]->input == "00--1-"_bi);
			TestCheck(pla.products->data[7]->output == "1"_bi);
			TestCheck(pla.products->data[8]->input == "0--010"_bi);
			TestCheck(pla.products->data[8]->output == "1"_bi);
			TestCheck(pla.products->data[9]->input == "01-110"_bi);
			TestCheck(pla.products->data[9]->output == "1"_bi);
			TestCheck(pla.products->data[10]->input == "01--00"_bi);
			TestCheck(pla.products->data[10]->output == "1"_bi);
			TestCheck(pla.products->data[11]->input == "0-1-00"_bi);
			TestCheck(pla.products->data[11]->output == "1"_bi);
			TestCheck(pla.products->data[12]->input == "000-00"_bi);
			TestCheck(pla.products->data[12]->output == "1"_bi);
		}
	}

EndTestUnit(DNF0)
