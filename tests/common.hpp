//
// Created by egor9814 on 06 Aug 2021.
//

#ifndef PLASLIB_COMMON_HPP
#define PLASLIB_COMMON_HPP

#include <stest.hpp>
#include <pla.hpp>
#include <variant>
#include <fstream>
#include <functional>

#define PLATestUnit(NAME) FixtureTestUnit(NAME, pla::Fixture)

#define __str0(X) #X
#define __str(X) __str0(X)
#define __defer_cat0(X,Y) X##Y
#define __defer_cat(X,Y) __defer_cat0(X,Y)
#define __defer(NAME) struct NAME { std::function<void()> f; ~NAME() { f(); } } __defer_cat(NAME, __instance); \
	__defer_cat(NAME, __instance).f =
#define defer __defer(__defer_cat(__defer__, __LINE__))

namespace pla {

	struct Fixture {

		class Result {
			std::variant<File, std::string> data;

		public:
			explicit Result(const File &f) : data(f) {}
			explicit Result(const std::string &error) : data(error) {}

			Result(const Result &) = delete;
			Result &operator=(const Result &) = delete;

			[[nodiscard]] bool isError() const {
				return data.index() == 1;
			}

			[[nodiscard]] const File &getFile() const {
				return std::get<File>(data);
			}

			[[nodiscard]] const std::string &getError() const {
				return std::get<std::string>(data);
			}
		};

		[[nodiscard]] Result openPLA(const std::string &path) const {
			try {
				std::ifstream input(path);
				if (!input) throw std::runtime_error("cannot open file: " + path);
				defer [&input] { input.close(); };
				File f{15};
				input >> f;
				return Result(f);
			} catch (const std::exception &err) {
				return Result(err.what());
			} catch (...) {
				return Result("unknown error at: " __FILE__ ":" __str(__LINE__));
			}
		}

	};

}

#endif //PLASLIB_COMMON_HPP
