//
// Created by egor9814 on 04 Aug 2021.
//

#include "common.hpp"

PLATestUnit(DNF0)

	AutoTestCase() {
		using namespace bitops::literals;
		if (auto r = openPLA("dnf_0.pla"); r.isError()) {
			static constexpr bool notError = false;
			TestCheckMessage(notError, r.getError());
		} else {
			const auto &pla = r.getFile();
			TestCheck(pla.inputLabels.size() == 6);
			TestCheck(pla.outputLabels.empty());
			TestCheck(pla.products->size() == 10);
			TestCheck(pla.products->data[0]->input == "000-0-"_bi);
			TestCheck(pla.products->data[0]->output == "1"_bi);
			TestCheck(pla.products->data[1]->input == "010-0-"_bi);
			TestCheck(pla.products->data[1]->output == "1"_bi);
			TestCheck(pla.products->data[2]->input == "--010-"_bi);
			TestCheck(pla.products->data[2]->output == "1"_bi);
			TestCheck(pla.products->data[3]->input == "-10--0"_bi);
			TestCheck(pla.products->data[3]->output == "1"_bi);
			TestCheck(pla.products->data[4]->input == "0---10"_bi);
			TestCheck(pla.products->data[4]->output == "1"_bi);
			TestCheck(pla.products->data[5]->input == "-1-0-0"_bi);
			TestCheck(pla.products->data[5]->output == "1"_bi);
			TestCheck(pla.products->data[6]->input == "1-11--"_bi);
			TestCheck(pla.products->data[6]->output == "1"_bi);
			TestCheck(pla.products->data[7]->input == "00--00"_bi);
			TestCheck(pla.products->data[7]->output == "1"_bi);
			TestCheck(pla.products->data[8]->input == "1-1-1-"_bi);
			TestCheck(pla.products->data[8]->output == "1"_bi);
			TestCheck(pla.products->data[9]->input == "1--010"_bi);
			TestCheck(pla.products->data[9]->output == "1"_bi);
		}
	}

EndTestUnit(DNF0)
