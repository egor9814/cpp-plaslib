//
// Created by egor9814 on 05 Aug 2021.
//

#ifndef PLASLIB_PLA_HPP
#define PLASLIB_PLA_HPP

#include <bitops/interval.hpp>
#include <memory>
#include <vector>
#include <string>
#include <map>
#include <set>

namespace pla {

	using bitops::BitInterval;

	struct BitIntervalComparator {
		bool operator()(const BitInterval &a, const BitInterval &b) const {
			// TODO: make better
			return a.toString() < b.toString();
		}
	};

	template <typename T>
	using BitIntervalMap = std::map<BitInterval, T, BitIntervalComparator>;


	using DNF = std::vector<BitInterval>;
	using RawDNF = std::vector<BitInterval const *>;

	struct Product;
	using ProductPtr = std::shared_ptr<Product>;
	struct Product {
		BitInterval input, output;

		Product(BitInterval input, BitInterval output) noexcept;

		static ProductPtr create(const BitInterval &input, const BitInterval &output);
	};

	struct ProductList;
	using ProductListPtr = std::shared_ptr<ProductList>;
	struct ProductList {
		std::vector<ProductPtr> data{};

//		[[nodiscard]] bool contains(const ProductPtr &p) const;

		void append(const ProductPtr &p);

		ProductPtr append(const BitInterval &input, const BitInterval &output);

		[[nodiscard]] size_t size() const;

		[[nodiscard]] BitIntervalMap<std::vector<BitInterval>> group() const;

		explicit ProductList(size_t capacity = 1);

		static ProductListPtr create(size_t capacity = 1);
	};

	struct File {
		std::vector<std::string> inputLabels, outputLabels;
		ProductListPtr products;

		File(size_t capacity = 1);

		[[nodiscard]] BitIntervalMap<DNF> toDNFs() const;

		friend std::istream &operator>>(std::istream &in, File &f);

		friend std::ostream &operator<<(std::ostream &out, const File &f);
	};

	/*struct Reader {
		int inputs, outputs, productsCount;
		File &out;

		void fromLines(const std::vector<std::string> &lines);
	};*/

}

#endif //PLASLIB_PLA_HPP
