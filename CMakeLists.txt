cmake_minimum_required(VERSION 3.19)
project(plaslib LANGUAGES CXX)

add_subdirectory(libs)

file(GLOB_RECURSE lib_sources
        "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp"
)
add_library(plaslib STATIC ${lib_sources})
target_compile_features(plaslib PUBLIC cxx_std_20)
target_include_directories(plaslib PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include)
bitops_link_target(plaslib)

set_property(TARGET plaslib PROPERTY API_HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/include")

function(plaslib_link_target TARGET)
    get_property(plaslib_headers TARGET plaslib PROPERTY API_HEADERS)
    target_include_directories(${TARGET} PRIVATE ${plaslib_headers})
    target_link_libraries(${TARGET} plaslib)
endfunction(plaslib_link_target)

# <<<Tests Begin>>> #
if (PLASLIB_TESTS_ENABLED)
    add_definitions(-DPLASLIB_TESTS_ENABLED)
    include(FetchContent)
    FetchContent_Declare(
            stest
            GIT_REPOSITORY "https://gitlab.com/egor9814/cpp-stest"
            GIT_TAG "9352e31412b66f7e0c293bcfd48929e67d03a27e"
    )
    FetchContent_MakeAvailable(stest)
    add_subdirectory(tests)
    stest_link_target(plaslib)
endif(PLASLIB_TESTS_ENABLED)
# <<<Tests End>>> #
